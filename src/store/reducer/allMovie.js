import {
    GET_ALL_MOVIE_BEGIN,
    GET_ALL_MOVIE_SUCCESS,
    GET_ALL_MOVIE_FAIL,

    GET_BY_GENRE_BEGIN,
    GET_BY_GENRE_SUCCESS,
    GET_BY_GENRE_FAIL,

    GET_MOVIE_BY_GENRE_BEGIN,
    GET_MOVIE_BY_GENRE_SUCCESS,
    GET_MOVIE_BY_GENRE_FAIL,

    GET_PAGE_BEGIN,
    GET_PAGE_SUCCESS,
    GET_PAGE_FAIL,
} from "../action/actionTypes"

const initialState = {
    loading: false,
    error: null,
    allMovies: [],
    byGenre: [],
    filterGenre: [],
    activePage: 1,
    currentPage: 1,
    currentLimit: 5,
    pagin: {},
    genre: 'all'
     
}

export const AllMovie = ( state = initialState, action ) => {
    const {type, payload, error, genre, activePage} = action;
    switch (type) {
        default:
            return {
                ...state,
            }
        case GET_ALL_MOVIE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            }
        case GET_ALL_MOVIE_SUCCESS:
            return {
                ...state,
                loading: false,
                allMovies: payload.docs,
                pagin: payload,
                genre: genre,
                activePage: activePage,
                error: null,
            }
        case GET_ALL_MOVIE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                allMovies: [],  
            }
        case GET_MOVIE_BY_GENRE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            }
        case GET_MOVIE_BY_GENRE_SUCCESS:
            return {
                loading: false,
                filterGenre: payload.docs,
                pagin: payload,
                genre: genre,
                activePage: activePage,
                error: null,  
            }
        case GET_MOVIE_BY_GENRE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                filterGenre: [],
            }
    }
};

export const ByGenre = ( state = initialState, action ) => {
    const {type, payload, error} = action;
    switch (type) {
        default:
            return {
                ...state,
            }
        case GET_BY_GENRE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            }
        case GET_BY_GENRE_SUCCESS:
            return {
                ...state,
                loading: false,
                byGenre: payload,
                error: null,
            }
        case GET_BY_GENRE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                
            }
    }
};

export const PaginPage = ( state = initialState, action ) => {
    const {type, payload, error} = action;
    switch (type) {
        default:
            return {
                ...state,
            }
        case GET_PAGE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            }
        case GET_PAGE_SUCCESS:
            return {
                ...state,
                loading: false,
                pagin: payload,
                error: null,
            }
        case GET_PAGE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                
            }
    }
};

