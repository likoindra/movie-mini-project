import React from 'react'
import './DetailPage.css'
import { Provider } from 'react-redux'
import FooterComponent from '../../components/footer/footer'
import Header from '../../components/Navbar/Navbar'
import store from '../../store'
import Banner from './Banner/Banner'
import { Tabs, Tab } from 'react-bootstrap'
import Overview from './overview/overview'
import Review from './Review/Review'
import Character from './character/character';


function DetailPage() {
    return (
      <>
      <Provider store={store}>
        <Header/>
        <Banner />
        <div className="overview-button">
            <Tabs defaultActiveKey="overview" id="movie-detail-tab" className="overview-movie-tab">
              <Tab eventKey="overview" title="Overview">
                <Overview />
              </Tab>
              <Tab eventKey="characters" title="Characters">
                <Character />
              </Tab>
              <Tab eventKey="review" title="Review">
                <Review />
              </Tab>
            </Tabs>
        </div>
        <FooterComponent />
      </Provider>
      </>
    )
}

export default DetailPage;
