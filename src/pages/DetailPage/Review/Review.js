import React from 'react'
import { Nav, Form } from 'react-bootstrap'
import './review.css'
import userPic from './assets/profile.jpg'
import StarRating from './component/starRating'
import LoadMore from './component/loadMore'
import store from '../../../store'
import { Provider } from 'react-redux'
import ReviewDataUser from './component/ReviewDataUser'


function Review() {
    return (
        <div className="review-container"> 
            <div className="review-user">
                <img className="user-pic" src={userPic} alt="user pic"/>
                <div className="user-item">
                    <h4>Bill The Cat</h4>
                    <StarRating />
                    <Form.Control as="textarea" rows={5} placeholder="Leave a review"/>
                </div>
            </div>
            <div className="review-input">
               <Provider store={store}>
                   <ReviewDataUser />
               </Provider>
            </div>
            
            <div className="load-more">
                <LoadMore />
            </div>
            
        </div>
    )
}

export default Review
