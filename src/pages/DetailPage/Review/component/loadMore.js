import React, { useState, useEffect } from 'react'
import { Button } from 'react-bootstrap'

function LoadingButton() {
    return new Promise((resolve) => setTimeout(resolve, 2000));
}

function LoadMore() {
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (isLoading) {
        LoadingButton().then(() => {
        setLoading(false);
      });
    }
  }, [isLoading]);

  const handleClick = () => setLoading(true);

  return (
    <Button
      style={{backgroundColor: "#EB507F", borderColor: "#EB507F"}}
      disabled={isLoading}
      onClick={!isLoading ? handleClick : null}
    >
      {isLoading ? 'Loading…' : 'Load More'}
    </Button>
  );
}

export default LoadMore
