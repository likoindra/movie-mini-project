import React from 'react'
import Header from '../../components/Navbar/Navbar'
import CarouselContainer from './carousel/carousel'
import Category from './category/category'
import FooterComponent from '../../components/footer/footer'

function HomePage() {
    return (
        <div>
          <Header/>
          <CarouselContainer />
          <Category />
          <FooterComponent />
        </div>
    )
}

export default HomePage
