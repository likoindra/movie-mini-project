import React, { useEffect, useState } from 'react'
import { Pagination } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux'
import { GetAllMovie, GetMovieByGenre, GetPagin } from '../../../store/action/allMovie'
import "./pagination.scss"


function PaginationHomepage() {
    const dispatch = useDispatch()
    const loading = useSelector(state => state.PaginPage.loading)
    const pagination = useSelector(state =>state.AllMovie) 

    const ClickPage = (page=1) => {
        if (pagination.allMovies == null){
            dispatch(GetMovieByGenre(pagination.genre, page))
        } else {
            dispatch(GetAllMovie(page))
        }
    }

    
    const CreatePagin = (totalPage, active = 1) => {
        let paginationPage = (totalPage - active) > 10 ? 5 + active : totalPage
        
        let itemsPage = [];
        for (let number = active; number <= paginationPage; number++) {
        itemsPage.push(
            <Pagination.Item 
                key={number} 
                active={number === active}
                onClick={() => {
                    ClickPage(number) 
                }}
            >
                {number}
            </Pagination.Item>
        )}
        if (totalPage - active > 10 ) {
            itemsPage.push(
                <Pagination.Ellipsis />
            )
            for (let number = totalPage-4; number <= totalPage; number++) {
                itemsPage.push(
                    <Pagination.Item 
                        key={number} 
                        active={number === active}
                        onClick={() => {
                            ClickPage(number) 
                        }}
                    >
                        {number}
                    </Pagination.Item>
                )
            }
        }
        return itemsPage;
    }
  

    return ( 
        <div className="pagin-container">
            <Pagination className="pagin-item">
            {
                loading ? (
                'loading...') : (                     
                    <>
                        <Pagination.First
                            onClick={() => {
                                ClickPage(1)
                            }}
                        />
                        <Pagination.Prev
                            onClick={() => {
                                ClickPage(pagination.pagin.prevPage)
                            }}
                        />
                        {CreatePagin(pagination.pagin.totalPages, pagination.activePage)}                                              
                        <Pagination.Next
                          onClick={() => {
                            ClickPage(pagination.pagin.nextPage);
                          }}
                        />
                        <Pagination.Last
                          onClick={() => {
                            ClickPage(pagination.pagin.totalPages);
                          }}
                        />
                    </> )}
            </Pagination> 
        </div>
    )
}

export default PaginationHomepage
